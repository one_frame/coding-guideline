# **Sample環境メモ**
<!-- TOC -->autoauto- [**Sample環境メモ**](#sample環境メモ)auto  - [**概要**](#概要)auto  - [**Setup**](#setup)auto    - [**npmでyarnをグローバルインストール**](#npmでyarnをグローバルインストール)auto    - [**gulpのグローバルインストール**](#gulpのグローバルインストール)auto    - [**パッケージのインストール**](#パッケージのインストール)auto  - [**Commands**](#commands)auto  - [**Editor（VSCode）の設定**](#editorvscodeの設定)auto    - [**1. stylelint インストール**](#1-stylelint-インストール)auto    - [**2. .stylelintrc.json ファイルを作成**](#2-stylelintrcjson-ファイルを作成)auto    - [**2. Extension（拡張機能） のインストール**](#2-extension拡張機能-のインストール)auto    - [**3. VSCodeの settings.json への追加**](#3-vscodeの-settingsjson-への追加)auto    - [**4. prettier-stylelint パッケージのインストール**](#4-prettier-stylelint-パッケージのインストール)auto    - [**5. コーディング**](#5-コーディング)autoauto<!-- /TOC -->

## **概要**

`gulp`を使い、`scss`をビルドします。ソースマップ作成、ブラウザプレフィックス、minifyを同時に行います。

まず、`sample` フォルダ配下の `assets` 内でインストールしてから、コマンドを実行します。

`assets/style` 配下の `SCSS,CSS`をビルドすると、`sample/public/css` に cssが配置されます。


## **Setup**


### **npmでyarnをグローバルインストール**

```terminal
$ npm install -g yarn
```

### **gulpのグローバルインストール**

`package.json`があるディレクトリで

```terminal
$ yarn global gulp
```

### **パッケージのインストール**

`package.json`があるディレクトリで

```terminal
$ yarn install
```

## **Commands**

| command    | script                          |
|:-----------|:--------------------------------|
| gulp scss  | style配下にあるscss,cssをビルド |
| gulp watch | style配下にあるscss,cssを監視   |

これまでの作業で、基本のビルドができるようになります。


追加で、できればチーム全員で記述ルールを統一するために下記のEditor設定をしてください。


## **Editor（VSCode）の設定**

リンター（`stylelint`）のエラーをリアルタイムにエディタのコンソールに表示してたいのと、整形（`prettier`）を保存前にもしたいため、エディター側に設定しようと思います。

`prettier` の整形ルールは `stylelint` の設定を使うようにするため、`prettier-stylelint` を利用します。

VSCode以外もほぼ対応できると思います。


### **1. stylelint インストール**

本 `package.json` に含まれているので飛ばしていいです。

```terminal
yarn add --dev stylelint //本体
yarn add --dev stylelint-config-standard //標準ルール
yarn add --dev stylelint-config-recess-order //プロパティソート設定
```

### **2. .stylelintrc.json ファイルを作成**
`stylelint` の設定を書きます。

すでに含まれているので飛ばしていいです。

```json
{
  "extends": ["stylelint-config-standard", "stylelint-config-recess-order"],
  "rules": {
    "indentation": 2,
    "declaration-colon-newline-after": null,
    "value-list-comma-newline-after": "never-multi-line"
  }
}
```


### **2. Extension（拡張機能） のインストール**

* stylelint
* prettier
* EditorConfig for VS Code
  * ルートにある、`.editorconfig`は今後いろいろなプロジェクトで使用されると思われるので、インストールしておくといいです。

他の対応エディタはこちら

[Prettier Editor Integration](https://prettier.io/docs/en/editors.html)


### **3. VSCodeの settings.json への追加**

```json
{
  ...
  "editor.formatOnSave": true, //保存時に整形
  "prettier.stylelintIntegration": true, //stylelintの設定を使う
  "prettier.printWidth": 1000, //勝手に改行しないように

  "css.validate": false, //組み込みのバリデート（チェック）をオフ
  "scss.validate": false //組み込みのバリデート（チェック）をオフ
  ...
}
```
を設定して、保存時にオートフォーマットされるようにします。ただし `prettier` の機能だけでは足りないので `stylelint` と連携します。これも本リポジトリに含まれているので、基本は設定されています。


### **4. prettier-stylelint パッケージのインストール**

本 `package.json` に含まれているので飛ばしていいです。

```terminal
yarn add --dev prettier-stylelint
```

### **5. コーディング**

保存時(cmd+s)、もしくは、右クリックメニューのドキュメントのフォーマット（option+shift+f）で整形されるようになります。

