const gulp = require("gulp");
const plumber = require("gulp-plumber");
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const cleancss = require("gulp-clean-css");
const autoprefixer = require("gulp-autoprefixer");
const notify = require("gulp-notify");
const browsersync = require('browser-sync');
const reload = browsersync.reload;

const paths = {
  src_scss: "./sample/assets/style/**/*.scss",
  src_css: "./sample/assets/style/**/*.css",
  dest: "./sample/public/css/"
};
const serverpaths = {
  static_root: './sample/public',
  static_start: 'index.html',
  watch_public: './sample/public/**/*'
};

// -----------------------------
// Build
// -----------------------------
gulp.task('scss', function() {
  return gulp
    .src(paths.src_scss)
    .pipe(
      plumber({
        errorHandler: notify.onError('Error: <%= error.message %>')
      })
    )
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        outputStyle: 'expanded'
      })
    )
    .pipe(
      cleancss({ debug: true }, function(details) {
        console.log(details.name + ': ' + details.stats.originalSize + ' > ' + details.stats.minifiedSize);
      })
    )
    .pipe(
      autoprefixer({
        browsers: ['> 1%', 'last 2 versions', 'ie >= 11', 'Android >= 4.4'],
        cascade: false
      })
    )
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.dest));
});

// -----------------------------
// Localserver with live reload
// localhost:3019
// -----------------------------
gulp.task('server', function() {
  browsersync.init({
    port: 3010,
    server: {
      baseDir: serverpaths.static_root
    },
    startPath: serverpaths.static_start
  });
  gulp.watch(serverpaths.watch_public, reload);
});

// -----------------------------
// Watch SCSS
// -----------------------------
gulp.task('watch', ['scss'], function() {
  gulp.watch([paths.src_scss, paths.src_css], ['scss']);
});

// -----------------------------
// default task
// -----------------------------
gulp.task('default', ['server', 'watch']);
